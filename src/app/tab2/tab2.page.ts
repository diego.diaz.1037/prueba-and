import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

import { ActionSheetController } from '@ionic/angular';

import { PopoverController } from '@ionic/angular';
import { PopoverComponent } from '../popover/popover.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  private title:any;
  private overview:any;
  private score:any;
  private date:any;
  private image:any;

  constructor( private router:Router,
               private storage: Storage,
               public actionSheetController: ActionSheetController,
               public popoverController: PopoverController) {

    this.storage.get('title').then((val) => { this.title = val; })
    this.storage.get('overview').then((val) => { this.overview = val; })
    this.storage.get('score').then((val) => { this.score = val; })
    this.storage.get('date').then((val) => { this.date = val; })
    this.storage.get('image').then((val) => { this.image = val; })

  }


  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Compartir',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Twitter',
        icon: 'logo-twitter',
        handler: () => {
          console.log('Twitter clicked');
        }
      }, {
        text: 'Correo',
        icon: 'mail-outline',
        handler: () => {
          console.log('Email clicked');
        }
      }, {
        text: 'Whatsapp',
        icon: 'logo-whatsapp',
        handler: () => {
          console.log('Whatsapp clicked');
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }


  top(){
    this.router.navigate(['/top'])
  }
  
}

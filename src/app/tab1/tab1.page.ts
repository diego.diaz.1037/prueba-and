import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';

import { ActionSheetController } from '@ionic/angular';

import { PopoverController } from '@ionic/angular';
import { PopoverComponent } from '../popover/popover.component';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  public movies;
  public results;

  private titles:any[] = [];
  private overviews:any[] = [];
  private scores:any[] = [];
  private dates:any[] = [];
  private images:any[] = [];

  public URLimage:any = "https://image.tmdb.org/t/p/w500"

  constructor(private router:Router,
              private storage: Storage,
              public toastController: ToastController,
              public actionSheetController: ActionSheetController,
              public popoverController: PopoverController
              ) {

    this.movies= JSON.parse(this.router.getCurrentNavigation().extras.state.movies)
    console.log("MOVIES: ", (this.movies))
    this.results = this.movies.results;
    console.log( this.results)


    //TITLES
    for ( let i=0; i< this.results.length;i++){
      let title = this.results[i].title;
      this.titles.push(title);
    }
    console.log("MOVIES titles: ", this.titles)

    //OVERVIEW
    for ( let i=0; i< this.results.length;i++){
      let overview = this.results[i].overview;
      this.overviews.push(overview);
    }
    console.log("MOVIES overviews: ", this.overviews)

    //SCORES
    for ( let i=0; i< this.results.length;i++){
      let score = this.results[i].vote_average;
      this.scores.push(score);
    }
    console.log("MOVIES scores: ", this.scores)

    //DATES
    for ( let i=0; i< this.results.length;i++){
      let date = this.results[i].release_date;
      this.dates.push(date);
    }
    console.log("MOVIES dates: ", this.dates)

      //IMAGES
      for ( let i=0; i< this.results.length;i++){
        let image = this.results[i].backdrop_path;
        this.images.push(this.URLimage + image);
      }
      console.log("MOVIES images: ", this.images)

   

  }

  Download(i){
    console.log(this.results[i])

    this.storage.set('title', this.titles[i]);
    this.storage.set('overview', this.overviews[i]);
    this.storage.set('score', this.scores[i]);
    this.storage.set('date', this.dates[i]);
    this.storage.set('image', this.images[i]);

    this.presentToast();
  }


  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Contenido Descargado.',
      duration: 2000
    });
    toast.present();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Compartir',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Twitter',
        icon: 'logo-twitter',
        handler: () => {
          console.log('Twitter clicked');
        }
      }, {
        text: 'Correo',
        icon: 'mail-outline',
        handler: () => {
          console.log('Email clicked');
        }
      }, {
        text: 'Whatsapp',
        icon: 'logo-whatsapp',
        handler: () => {
          console.log('Whatsapp clicked');
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }
  
  download(){
    this.router.navigate(['/download'])
  }


}

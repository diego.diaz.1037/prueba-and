import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent  {

  constructor(private router: Router) { }

  LogOut(){
    this.router.navigate(['/login']);
  }

}
